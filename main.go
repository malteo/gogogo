package main

import "fmt"
import "net/http"

func main() {
    http.HandleFunc("/", hello)
    http.ListenAndServe(":8080", nil)
}

func hello(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("GIAO"))
    fmt.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
}
